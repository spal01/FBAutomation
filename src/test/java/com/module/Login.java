package com.module;

import java.io.File;
import java.lang.reflect.Method;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Login {
String relativePath="driver/";
String driverChoice="chromedriver.exe";
String absolutePath="";
File fl=null;
ChromeDriver cdriver=null;

ExtentReports extentReports=null;	
ExtentTest logger=null;

String url="";

XSSFWorkbook wb=null;
XSSFSheet sheet=null;
Row row=null;
Cell cell=null;



@BeforeTest
public void setUp(){
	absolutePath=new File(relativePath+driverChoice).getAbsolutePath();
	System.setProperty("webdriver.chrome.driver", absolutePath);
	cdriver=new ChromeDriver();
	
	
	extentReports=new ExtentReports("D://Workspace//WorkSpace1//FBAutomation//test-output//STExtentReport.html",true);
	
	//extentReports=new ExtentReports("D://Workspace//WorkSpace1//FBAutomation//target//surefire-reports//STExtentReport.html",true);
	extentReports.addSystemInfo("Host Name","Software Testing Material");
	extentReports.loadConfig(new File("D://Workspace//WorkSpace1//FBAutomation//extent-config.xml"));
  
	
	
}


@BeforeMethod
public void beforeMethod(ITestResult result){
	logger=extentReports.startTest(result.getName());
}

@Test
  public void openUrl(){
	
	cdriver.get("http://www.flipkart.com");
	String expectedTitle="Online Shopping Site for Mobiles, Fashion, Books, Electronics, Home Appliances and More";
	String actualTitle=cdriver.getTitle();
	Assert.assertEquals(expectedTitle,actualTitle );
	logger.log(LogStatus.PASS, "Test Case is passed");
	
  }

@Test(dependsOnMethods={"openUrl"})
public void loginOperation(){
	
}



@AfterMethod(alwaysRun=true)
public void afterMethod(ITestResult result){
	if(result.getStatus()==ITestResult.SUCCESS){
		logger.log(LogStatus.PASS, "Passed Test Method " + result.getName());
	}
	
if(result.getStatus()==ITestResult.FAILURE){
	logger.log(LogStatus.FAIL, "Failed Test Method " + result.getName());
	}
	
if(result.getStatus()==ITestResult.SKIP){
	logger.log(LogStatus.SKIP, "Skipped Test Method " + result.getName());
}
	
extentReports.endTest(logger);
	
}


@AfterTest
public void endReport(){
	extentReports.flush();
	extentReports.close();;
}


}
